package com.example.memorina
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private val pareCard = 8
    private lateinit var cardViews: MutableList<ImageView>
    private var curPare: ImageView? = null
    private var pareFound = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        cardViews = mutableListOf()
        for (i in 0 until pareCard * 2) {
            val cardId = resources.getIdentifier("card_$i", "id", packageName)
            val card = findViewById<ImageView>(cardId)
            card.tag = i % pareCard
            card.setOnClickListener { onCardClicked(card) }
            cardViews.add(card)
        }
        newGame()
    }
    private fun onCardClicked(card: ImageView) {
        if (card == curPare || card.drawable == getCardBackDrawable()) {
            return
        }
        card.setImageResource(getImageForCard(card.tag as Int))
        if (curPare == null) {
            curPare = card
        } else {
            val tag1 = curPare!!.tag as Int
            val tag2 = card.tag as Int
            if (tag1 == tag2) {
                pareFound++
                curPare!!.isClickable = false
                card.isClickable = false
                curPare = null

                if (pareFound == pareCard) {
                    val toast = Toast.makeText(
                        applicationContext,
                        "You are the winner!", Toast.LENGTH_SHORT
                    )
                    toast.show()
                }
            } else {
                GlobalScope.launch(Dispatchers.Main) {
                    delay(700)
                    curPare!!.setImageResource(R.drawable.card_back)
                    card.setImageResource(R.drawable.card_back)
                    curPare = null
                }
            }
        }
    }
    private fun doRandom() {
        val tags = MutableList(pareCard * 2) { it % pareCard }
        tags.shuffle()
        cardViews.forEachIndexed { index, card ->
            card.tag = tags[index]
        }
    }

    private fun closePares() {
        cardViews.forEach { card ->
            card.setImageResource(R.drawable.card_back)
            card.isClickable = true
        }
    }
    fun resetGame(v: View){
        newGame()
    }

    private fun newGame() {
        curPare = null
        pareFound = 0
        doRandom()
        closePares()
    }


    private fun getImageForCard(tag: Int): Int {
        return when (tag) {
            0 -> R.drawable.card_0
            1 -> R.drawable.card_1
            2 -> R.drawable.card_2
            3 -> R.drawable.card_3
            4 -> R.drawable.card_4
            5 -> R.drawable.card_5
            6 -> R.drawable.card_6
            7 -> R.drawable.card_7
            else -> R.drawable.ic_launcher_foreground
        }
    }

    private fun getCardBackDrawable(): Drawable {
        return resources.getDrawable(R.drawable.card_back, null)
    }


}